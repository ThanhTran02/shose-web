import React from "react";
import { FaMapMarkerAlt, FaPhone } from "react-icons/fa";
import { MdOutlineMailOutline } from "react-icons/md";
import { PiGenderIntersexLight } from "react-icons/pi";
import { RiUser3Fill } from "react-icons/ri";

const AccountPageContent = () => {
  return (
    <div className="w-full account_content border-[1px] border-black">
      <h2 className=" bg-black text-white py-2 px-6 text-xl">
        Thông tin tài khoản
      </h2>
      <div className="table-responsive p-2">
        <table className="table ">
          <tbody>
            <tr>
              <td className="flex gap-2 items-center">
                <RiUser3Fill />
                <span className="customize-text">Họ tên</span>
              </td>
              <td>Trần Thành</td>
            </tr>
            <tr>
              <td className="flex gap-2 items-center">
                <MdOutlineMailOutline />
                <span className="customize-text">Email</span>
              </td>
              <td>123456@gmail.com</td>
            </tr>
            <tr>
              <td className="flex gap-2 items-center">
                <PiGenderIntersexLight />
                <span className="customize-text">Giới tính</span>
              </td>
              <td>Nữ</td>
            </tr>
            <tr>
              <td className="flex gap-2 items-center">
                <FaPhone />
                <span className="customize-text">Số điện thoại</span>
              </td>
              <td>012345678</td>
            </tr>
            <tr>
              <td className="flex gap-2 items-center">
                <FaMapMarkerAlt />
                <span className="customize-text">Địa chỉ</span>
              </td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default AccountPageContent;
