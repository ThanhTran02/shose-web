import { Button } from "antd";
import React from "react";

const CardHomeCart = () => {
  return (
    <div className="grid grid-cols-5 gap-3 h-full items-center">
      <div className=" col-span-1 ">
        <img
          src="https://product.hstatic.net/1000365025/product/1_1210e36837dc4b2e809ce24f50093631.jpg"
          alt=""
        />
      </div>

      <div className="col-span-3 flex flex-col gap-1">
        <p>DC23 WHITE</p>
        <p>439,000₫</p>
        <p>Số lượng: 3</p>
      </div>
      <div className="col-span-1">
        <Button> xóa</Button>
      </div>
    </div>
  );
};

export default CardHomeCart;
