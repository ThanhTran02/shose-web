import React, { useState } from "react";
import { Button, Drawer } from "antd";
import { BsHeart } from "react-icons/bs";
import CardHomeCart from "./card.home.cart";

const AppWishLishDrawer = () => {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <div
        className="flex gap-2 items-center cursor-pointer"
        onClick={showDrawer}
      >
        <p>Yêu thích</p>
        <BsHeart />
      </div>

      <Drawer
        title="Danh mục sản phẩm yêu thích"
        onClose={onClose}
        open={open}
        placement="bottom"
      >
        <div className="h-[200px] ">
          <CardHomeCart />
        </div>
      </Drawer>
    </>
  );
};

export default AppWishLishDrawer;
