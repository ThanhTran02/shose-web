"use client";
import React, { useEffect, useState } from "react";
import Link from "next/link";

import { Modal, Tabs } from "antd";
import { LiaUserEditSolid } from "react-icons/lia";

import ForgetPasswordForm from "../formHeaderGuest/ForgetPasswordForm";
import { SignInForm } from "../formHeaderGuest";
import SignUpForm from "../formHeaderGuest/SignUpForm";
import { useAppDispatch } from "@/lib/hook";
import { loginThunk } from "@/lib/features/userThunk";
import { useAuth } from "@/hook";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

const AppModelLogin = () => {
  const dispatch = useAppDispatch();
  const onFinishLogin = async (values: any) => {
    console.log("Success:", values);
    dispatch(loginThunk(values))
      .unwrap()
      .then(() => {
        toast.success("Đang nhập thành công !");
        handleCancel();
      })
      .catch((error: any) => {
        toast.error("Tài khoản hoặc mật khẩu không chính xác !");
      });
  };
  const ItemForm = [
    {
      label: "Đăng Nhập",
      key: "dangNhap",
      children: <SignInForm onFinish={onFinishLogin} />,
    },
    {
      label: "Quên mật khẩu",
      key: "quenMatKhau",
      children: <ForgetPasswordForm />,
    },
    { label: "Đăng ký", key: "dangKy", children: <SignUpForm /> },
  ];
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const { user } = useAuth();
  const router = useRouter();
  useEffect(() => {
    //@ts-ignore
    if (user?.role === "admin") {
      router.push("/admin");
    }
  }, [user]);
  return (
    <>
      {user !== null ? (
        <Link
          href={"./account"}
          className="flex gap-2 items-center cursor-pointer"
        >
          <p>Tài khoản</p>
          <LiaUserEditSolid />
        </Link>
      ) : (
        <div
          className="flex gap-2 items-center cursor-pointer"
          onClick={showModal}
        >
          <p>Tài khoản</p>
          <LiaUserEditSolid />
        </div>
      )}
      <Modal
        footer={false}
        open={isModalOpen}
        onCancel={handleCancel}
        width={700}
      >
        <Tabs tabPosition={"left"} items={ItemForm} />
      </Modal>
    </>
  );
};

export default AppModelLogin;
