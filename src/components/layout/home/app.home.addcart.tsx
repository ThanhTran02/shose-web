import React, { useState } from "react";
import { Drawer } from "antd";
import { CiShoppingCart } from "react-icons/ci";
import CardHomeCart from "./card.home.cart";
import Link from "next/link";

const AppAddCart = () => {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  const arr = new Array(2).fill(0);
  return (
    <>
      <div
        className="flex gap-2 items-center cursor-pointer"
        onClick={showDrawer}
      >
        <p>Giỏ hàng</p>
        <CiShoppingCart />
      </div>
      <Drawer
        title="GIỎ HÀNG"
        onClose={onClose}
        open={open}
        footer={
          <div className="">
            <div className="text-[#979696] text-[14px] flex justify-between items-center">
              <p>Thêm ghi chú</p>
              <Link href={"./cart"}>
                <p className="text-[#979696]">Xem chi tiết giỏ hàng</p>
              </Link>
            </div>
            <div className=" bg-red-500 active:bg-red-700 rounded-md text-white text-[16px] px-2 py-2 text-center mt-5 cursor-pointer duration-300">
              <p>
                THANH TOÁN <span>0₫</span>
              </p>
            </div>
          </div>
        }
      >
        <div className="flex justify-between flex-col h-full">
          <div className=" flex flex-col gap-5">
            {arr.map((_, index) => (
              <div key={index} className="border-b pb-3">
                <CardHomeCart />
              </div>
            ))}
          </div>
        </div>
      </Drawer>
    </>
  );
};

export default AppAddCart;
