"use client";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";
import { Button } from "antd";
import "../../carosel/carosel.scss";
const NextArrow = (props: any) => {
  return (
    <Button
      color="inherit"
      onClick={props.onClick}
      style={{
        position: "absolute",
        right: 0,
        top: "45%",
        zIndex: 2,
        width: 30,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <SlArrowRight />
    </Button>
  );
};

const PrevArrow = (props: any) => {
  return (
    <Button
      color="inherit"
      onClick={props.onClick}
      style={{
        position: "absolute",
        top: "45%",
        zIndex: 2,
        minWidth: 30,
        width: 30,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <SlArrowLeft />
    </Button>
  );
};
function ProductNavFor() {
  const settings = {
    // customPaging: function (i: number) {
    //   return (
    //     <a>
    //       <img src={`${baseUrl}/abstract0${i + 1}.jpg`} />
    //     </a>
    //   );
    // },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };
  return (
    <div className="slider-container">
      <Slider {...settings}>
        <div>
          <img
            src="https://product.hstatic.net/1000365025/product/1_1210e36837dc4b2e809ce24f50093631_master.jpg"
            className="w-[400px]"
          />
        </div>
        <div>
          <img
            src="https://product.hstatic.net/1000365025/product/2_ffbda9b6aee14b6cb6f327bc41a62ecd_master.jpg"
            className="w-[400px]"
          />
        </div>
        <div>
          <img
            src="https://product.hstatic.net/1000365025/product/3_1973465d863e4a74a90b5df4c1fbc313_master.jpg"
            className="w-[400px]"
          />
        </div>
        <div>
          <img
            src="https://product.hstatic.net/1000365025/product/4_01b411a93b45444a9bb631da199911c4_master.jpg"
            className="w-[400px]"
          />
        </div>
      </Slider>
    </div>
  );
}

export default ProductNavFor;
