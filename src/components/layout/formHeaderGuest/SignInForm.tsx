import React from "react";
import { Form, Input } from "antd";

export const SignInForm = ({ onFinish }: any) => (
  <Form
    name="basic"
    labelCol={{ span: 8 }}
    wrapperCol={{ span: 24 }}
    layout="vertical"
    onFinish={onFinish}
    autoComplete="off"
  >
    <Form.Item
      label="Tên đăng nhập"
      name="username"
      rules={[{ required: true, message: "Vui lòng nhập tên đăng nhập!" }]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="Mật khẩu"
      name="password"
      rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
    >
      <Input.Password />
    </Form.Item>

    <Form.Item>
      <button
        type="submit"
        className="w-full bg-black text-white py-2 font-semibold text-[16px] rounded-md"
      >
        ĐẶNG NHẬP
      </button>
    </Form.Item>
  </Form>
);

export default SignInForm;
