"use client";
import React, { useRef } from "react";
import { CiHeart } from "react-icons/ci";
import { Tooltip } from "antd";
import { useRouter } from "next/navigation";
import { Product } from "@/types";
export const CardProduct = ({ product }: { product: Product }) => {
  const router = useRouter();
  const heartRef = useRef(null);

  const handleClick = () => {
    // @ts-ignore
    if (heartRef.current) heartRef.current.focus();
  };
  return (
    <div className="p-2 bg-[#f4f4f4] card_product">
      <div className="flex flex-col items-center justify-center gap-2 rounded-lg bg-white p-3  relative z-1">
        <img
          className="w-[300px] rounded-lg  "
          src="//product.hstatic.net/1000365025/product/dincox22_5292dd822151473e8ae7911989ff976b_grande.jpg"
          data-src="//product.hstatic.net/1000365025/product/dincox22_5292dd822151473e8ae7911989ff976b_grande.jpg"
          alt=" E16 BLACK "
        />
        <p>{product.name}</p>
        <p className="text-red-400">
          {parseFloat(product.price).toLocaleString()}
        </p>
        <p>FREESHIP</p>
        <div className="flex items-center  justify-between gap-1 w-full  ">
          <button className="bg-[#eceff1]  active:bg-[#dae3ea] py-[5px] w-[45%] rounded-lg cursor-pointer button_overlay ">
            Xem nhanh
          </button>
          <button
            className=" bg-red-500 active:bg-red-800  py-[5px]  w-[45%] rounded-md text-white cursor-pointer button_overlay "
            onClick={() => router.push(`./products/${product.name}`)}
          >
            Mua ngay
          </button>
        </div>
        <div className="overlay absolute  w-[90%] p-3 top-1 right-1 flex items-end justify-end ">
          <div className="flex items-center justify-center bg-white rounded-full text-3xl p-1 cursor-pointer">
            <Tooltip placement="bottom" title="Yêu thích" color="red">
              <span ref={heartRef}>
                <CiHeart onClick={handleClick} />
              </span>
            </Tooltip>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardProduct;
