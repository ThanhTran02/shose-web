export interface Product {
  product_id: number;
  name: string;
  description: string;
  price: string;
  image: string | null;
  category_id: number;
  quantity: number;
  categories: {
    category_id: number;
    name: string;
  };
}

interface Order {
  order_id: number;
  user_id: number;
  product_id: number;
  order_date: string;
  quantity: number;
  product_size: number;
  total_price: number;
  products: Product;
}
