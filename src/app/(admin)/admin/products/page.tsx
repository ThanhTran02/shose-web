"use client";

import { SearchProps } from "antd/es/input";
import React, { useEffect, useRef, useState } from "react";
import { IoIosAddCircleOutline } from "react-icons/io";
import {
  Button,
  Form,
  type FormProps,
  Input,
  Modal,
  Table,
  Select,
  DatePicker,
  Space,
  InputNumber,
} from "antd";
import { toast } from "react-toastify";
import { CiEdit } from "react-icons/ci";
import { quanLySanPhamServices } from "@/server";
const { Search } = Input;

const page = () => {
  const [formData, setFormData] = useState({
    product_id: "",
    name: "",
    description: "",
    price: "",
    image: "",
    category_id: "",
    quantity: "",
  });

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const [searchProducts, setSearchProducts] = useState("");
  const [dataProducts, setDataProducts] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalUpdateOpen, setIsModalUpdateOpen] = useState(false);
  const warning = (id: any) => {
    Modal.warning({
      title: "Bạn thật sự muốn xóa sản phẩm này",
      onOk: async (id: any) => {
        try {
          // await quanLyDatSanPhamServices.deleteStaff(id);

          toast.success("Xóa sản phẩm thành công!");
          fetchData();
        } catch (error: any) {
          console.error("Error fetching data:", error);
          toast.error(error);
        }
      },
    });
  };
  //dữ liệu bảng
  const columns = [
    {
      title: "ID Sản phẩm",
      dataIndex: "product_id",
      key: "product_id",
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Mô tả",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Giá",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Hình ảnh",
      dataIndex: "image",
      key: "image",
      // render: (text) => text ? <img src={text} alt="Product Image" style={{ width: 50, height: 50 }} /> : "No Image"
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Tên danh mục",
      dataIndex: ["categories", "name"],
      key: "category_name",
    },
    {
      title: "Chỉnh sửa",
      key: "action",
      //@ts-ignore
      render: (_, record) => (
        <Space size="middle">
          <Button
            type="dashed"
            danger
            onClick={() => {
              showModallUpdate(record);
            }}
          >
            <CiEdit className="text-2xl" />
          </Button>

          <Button type="primary" danger onClick={() => warning(record.StaffID)}>
            Delete
          </Button>
        </Space>
      ),
    },
  ];
  //hàm lấy dữ liệu
  const fetchData = async () => {
    try {
      const { data } = await quanLySanPhamServices.getProductsByName(
        searchProducts
      );

      // @ts-ignore
      const newDataProducts = data.content.map((item) => ({
        ...item,
        key: item.product_id,
      }));
      setDataProducts(newDataProducts);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const onFinish: FormProps<any>["onFinish"] = async (values) => {
    console.log("Success:", values);
    try {
      // const res = await quanLyNhanVienServices.creatNewStaff(values);
      toast.success("Thêm sản phẩm thành công!");
      // console.log(res);
      handleCancel();
      fetchData();
    } catch (error) {
      console.error("Error create new products:", error);
    }
  };

  const onFinishUpdate = async () => {
    console.log("Success:", formData);
    const { product_id, ...newFormData } = formData;
    //@ts-ignore
    delete newFormData.key;
    try {
      // const res = await quanLyNhanVienServices.updateInfoStaff(
      //   StaffID,
      //   newFormData
      // );
      toast.success("Cập nhật thông tin sản phẩm thành công!");
      // console.log(res);
      handleCancelUpdate();
      fetchData();
    } catch (error) {
      console.error("Error create new Staff:", error);
    }
  };

  //modal thêm nhân viên
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  // modal sửa
  const handleCancelUpdate = () => {
    setIsModalUpdateOpen(false);
  };
  const showModallUpdate = (record: any) => {
    setIsModalUpdateOpen(true);
    setFormData(record);
  };

  //search
  const onSearch: SearchProps["onSearch"] = async (value, _e, info) => {
    setSearchProducts(value);
  };

  useEffect(() => {
    fetchData();
  }, [searchProducts]);

  return (
    <div>
      <div className="flex justify-between items-center mb-4">
        <Search
          placeholder="Tìm kiếm sản phẩm theo tên"
          allowClear
          onSearch={onSearch}
          style={{ width: 400 }}
        />
        <Button>
          <div className="flex gap-2 items-center" onClick={showModal}>
            <IoIosAddCircleOutline />
            <span>Thêm sản phẩm</span>
          </div>
        </Button>
      </div>
      {dataProducts && <Table dataSource={dataProducts} columns={columns} />}
      <Modal
        title="Thêm sản phẩm"
        open={isModalOpen}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          name="productForm"
          style={{ maxWidth: 600 }}
          labelCol={{ span: 8 }}
          onFinish={onFinish}
        >
          <Form.Item
            label="Tên sản phẩm"
            name="name"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mô tả"
            name="description"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Giá"
            name="price"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <InputNumber style={{ width: "100%" }} min={0} />
          </Form.Item>
          <Form.Item
            label="Ảnh"
            name="image"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Số lượng"
            name="quantity"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <InputNumber min={0} style={{ width: "100%" }} />
          </Form.Item>

          <Form.Item
            label="Danh mục"
            name="category_id"
            rules={[
              { required: true, message: "Không được bỏ trống thông tin này!" },
            ]}
          >
            <Select
              style={{ width: "100%" }}
              options={[
                { value: 1, label: "Giày nam" },
                { value: 2, label: "Giày nữ" },
              ]}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Thêm sản phẩm
            </Button>
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        title="Sửa thông tin sản phẩm"
        open={isModalUpdateOpen}
        onCancel={handleCancelUpdate}
        footer={false}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            onFinishUpdate();
          }}
        >
          <div className="flex items-start flex-col justify-center gap-3 ">
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">ID Sản phẩm:</p>
              <input
                type="text"
                className="border col-span-2 bg-white border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1 text-center "
                required
                value={formData.product_id}
                disabled
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Tên sản phẩm</p>
              <input
                type="text"
                value={formData.name}
                className="border col-span-2 bg-white border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1 text-center "
                required
                name="name"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full ">
              <p className="col-span-1 font-semibold ">Mô tả</p>
              <input
                type="text"
                value={formData.description}
                className="border col-span-2 bg-white border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1 text-center "
                required
                name="description"
                onChange={handleInputChange}
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Giá</p>
              <input
                type="number"
                className="border col-span-2 bg-white border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1 text-center "
                value={formData.price}
                name="price"
                onChange={handleInputChange}
                required
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Số lượng</p>
              <input
                type="number"
                className="border col-span-2 bg-white border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1 text-center "
                value={formData.quantity}
                name="quantity"
                onChange={handleInputChange}
                required
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Ảnh</p>
              <input
                type="text"
                className="border col-span-2 bg-white border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1 text-center "
                value={formData.image}
                name="image"
                onChange={handleInputChange}
                required
              />
            </div>
            <div className="grid grid-cols-3 gap-5 items-center w-full">
              <p className="col-span-1 font-semibold ">Danh mục</p>
              <select
                className="col-span-2 text-center bg-white border border-gray-300 text-gray-900 rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full py-1"
                value={formData.category_id}
                name="category_id"
                onChange={handleInputChange}
                required
              >
                <option value={1}>Giày nam</option>
                <option value={2}>Giày nữ</option>
              </select>
            </div>
            <button
              type="submit"
              className="font-semibold border-black border-[1px] rounded-md px-2 py-1 w-full hover:bg-slate-200 duration-300 active:bg-slate-500 "
            >
              Cập nhật
            </button>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default page;
