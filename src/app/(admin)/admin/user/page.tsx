"use client";
import React, { useState } from "react";
import { Button, Modal, Space, Table, Tag } from "antd";
import type { TableProps } from "antd";
import { CiEdit } from "react-icons/ci";
import { Input } from "antd";
import { SearchProps } from "antd/es/input";
import { IoIosAddCircleOutline } from "react-icons/io";

const { Search } = Input;
interface DataType {
  key: string;
  maNguoiDung: number;
  tenNguoiDung: string;
  thongTinChiTiet: string;
  phanQuyen: string;
}

const columns: TableProps<DataType>["columns"] = [
  {
    title: "Mã Người dùng",
    dataIndex: "maNguoiDung",
    key: "maNguoiDung",
    render: (text) => <p>{text}</p>,
  },
  {
    title: "Tên Người dùng",
    dataIndex: "tenNguoiDung",
    key: "tenNguoiDung",
  },
  {
    title: "Thông Tin Chi Tiết",
    dataIndex: "thongTinChiTiet",
    key: "thongTinChiTiet",
  },
  {
    title: "Phân Quyền",
    key: "phanQuyen",
    dataIndex: "phanQuyen",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <Button type="dashed" danger>
          <CiEdit className="text-2xl" />
        </Button>

        <Button type="primary" danger>
          Delete
        </Button>
      </Space>
    ),
  },
];

const data: DataType[] = [
  {
    key: "1",
    maNguoiDung: 1,
    tenNguoiDung: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    phanQuyen: "admin",
  },
  {
    key: "2",
    maNguoiDung: 2,
    tenNguoiDung: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    phanQuyen: "user",
  },
  {
    key: "3",
    maNguoiDung: 3,
    tenNguoiDung: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    phanQuyen: "user",
  },
  {
    key: "4",
    maNguoiDung: 4,
    tenNguoiDung: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    phanQuyen: "user",
  },
];

const UserAdminPage = () => {
  const onSearch: SearchProps["onSearch"] = (value, _e, info) =>
    console.log(info?.source, value);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className="bg-white p-4 h-full rounded-md ">
      <div className="flex justify-between items-center mb-4">
        <Search
          placeholder="Tìm sản phẩm theo tên"
          onSearch={onSearch}
          style={{ width: 400 }}
        />
        <Button>
          <div className="flex gap-2 items-center" onClick={showModal}>
            <IoIosAddCircleOutline />
            <span>Thêm Phòng</span>
          </div>
        </Button>
      </div>
      <Table columns={columns} dataSource={data} />
      <Modal
        title="Thêm người dùng mới"
        open={isModalOpen}
        onCancel={handleCancel}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </div>
  );
};
export default UserAdminPage;
