"use client";
import React from "react";
import { Button, Space, Table, Tag } from "antd";
import type { TableProps } from "antd";
import { CiEdit } from "react-icons/ci";
import { Input } from "antd";
import { SearchProps } from "antd/es/input";
import { IoIosAddCircleOutline } from "react-icons/io";

const { Search } = Input;
interface DataType {
  key: string;
  maSp: number;
  tenSp: string;
  thongTinChiTiet: string;
  soLuong: number;
}

const columns: TableProps<DataType>["columns"] = [
  {
    title: "Mã Sản Phẩm",
    dataIndex: "maSp",
    key: "maSp",
    render: (text) => <p>{text}</p>,
  },
  {
    title: "tên Sản Phẩm",
    dataIndex: "tenSp",
    key: "tenSp",
  },
  {
    title: "Thông Tin Chi Tiết",
    dataIndex: "thongTinChiTiet",
    key: "thongTinChiTiet",
  },
  {
    title: "Số lượng",
    key: "soLuong",
    dataIndex: "soLuong",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <Button type="dashed" danger>
          <CiEdit className="text-2xl" />
        </Button>

        <Button type="primary" danger>
          Delete
        </Button>
      </Space>
    ),
  },
];

const data: DataType[] = [
  {
    key: "1",
    maSp: 1,
    tenSp: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    soLuong: 10,
  },
  {
    key: "2",
    maSp: 1,
    tenSp: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    soLuong: 10,
  },
  {
    key: "3",
    maSp: 1,
    tenSp: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    soLuong: 10,
  },
  {
    key: "4",
    maSp: 1,
    tenSp: "John Brown",
    thongTinChiTiet: "New York No. 1 Lake Park",
    soLuong: 10,
  },
];

const ShopAdminPage = () => {
  const onSearch: SearchProps["onSearch"] = (value, _e, info) =>
    console.log(info?.source, value);
  return (
    <div className="bg-white p-4 h-full rounded-md ">
      <div className="flex justify-between items-center mb-4">
        <Search
          placeholder="Tìm sản phẩm theo tên"
          onSearch={onSearch}
          style={{ width: 400 }}
        />
        <Button>
          <div className="flex gap-2 items-center">
            <IoIosAddCircleOutline />
            <span>Thêm sản phẩm</span>
          </div>
        </Button>
      </div>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};
export default ShopAdminPage;
