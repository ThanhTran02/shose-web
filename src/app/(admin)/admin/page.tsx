"use client";
import React from "react";
import { Line, Pie } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  ArcElement,
} from "chart.js";
import { faker } from "@faker-js/faker";

ChartJS.register(ArcElement, Tooltip, Legend);

export const dataPie = {
  labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
  datasets: [
    {
      label: "# of Votes",
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(75, 192, 192, 1)",
        "rgba(153, 102, 255, 1)",
        "rgba(255, 159, 64, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top" as const,
    },
    title: {
      display: true,
      text: "Doanh Thu",
    },
  },
};

const labels = ["January", "February", "March", "April", "May", "June", "July"];

export const data = {
  labels,
  datasets: [
    {
      label: "Doanh thu",
      data: labels.map(() => faker.datatype.number({ min: -0, max: 100000 })),
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)",
    },
  ],
};
const AdminPage = () => {
  return (
    <div className="grid grid-cols-4 gap-4">
      <div className="p-4 bg-white rounded-md col-span-3">
        <h2 className="text-xl">Sales Analytics</h2>
        <div className="flex gap-5">
          <div>
            <p className="text-[#4e4e4e] text-[16px]">Tổng số người dùng</p>
            <p className="font-semibold text-[18px]">12</p>
          </div>
          <div>
            <p className="text-[#4e4e4e] text-[16px]">Số đơn hàng</p>
            <p className="font-semibold text-[18px]">12</p>
          </div>
          <div>
            <p className="text-[#4e4e4e] text-[16px]">Doang thu</p>
            <p className="font-semibold text-[18px]">12</p>
          </div>
        </div>
        <Line options={options} data={data} />
        <Line options={options} data={data} />
      </div>
      <div className="p-4 bg-white rounded-md col-span-1">
        <div>
          <h2 className="text-xl">Top sản phẩm</h2>
          <Pie data={dataPie} />
        </div>
      </div>
    </div>
  );
};

export default AdminPage;
