import Image from "next/image";
import React from "react";

const page = () => {
  const MY_BANK = {
    BANK_ID: 970418,
    ACCOUNT_NO: 1351142110,
    TEMPLATE: "compact",
    AMOUNT: "1000",
    DESCRIPTION: "ChUYEN TIEN",
    ACCOUNT_NAME: "",
  };
  const QR = `https://img.vietqr.io/image/${MY_BANK.BANK_ID}-${MY_BANK.ACCOUNT_NO}-${MY_BANK.TEMPLATE}.png?amount=${MY_BANK.AMOUNT}&addInfo=${MY_BANK.DESCRIPTION}&accountName=${MY_BANK.ACCOUNT_NAME}`;
  return (
    <div>
      <Image src={QR} alt="" height={200} width={200} />
    </div>
  );
};

export default page;
