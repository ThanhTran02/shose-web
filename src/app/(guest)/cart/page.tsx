"use client";
import React, { useEffect, useState } from "react";
import Link from "next/link";

import { HomeOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import { Breadcrumb } from "antd";

import QR from "@/components/QR/QR";
import { quanLyDatSanPhamServices } from "@/server";

const CartLayout = () => {
  const [products, setProducts] = useState<any>([]);
  const featchData = async () => {
    try {
      const { data } = await quanLyDatSanPhamServices.getAllOrders("1");
      //@ts-ignore
      setProducts(data.content);
    } catch (error) {
      console.log(error);
    }
  };
  console.log(products);

  useEffect(() => {
    featchData();
  }, []);
  return (
    <div className="mt-5 m-auto w-[1280px] min-h-[500px]">
      <Breadcrumb
        items={[
          {
            title: (
              <Link href={"/"}>
                <HomeOutlined />
                <span>Home</span>
              </Link>
            ),
          },
          {
            title: (
              <>
                <ShoppingCartOutlined />
                <span className="">Đơn hàng của bạn</span>
              </>
            ),
          },
        ]}
      />
      <div className=" my-4 border-t-[1px] border-[#212529]">
        {products.length > 0 ? (
          products.map((product: any) => (
            <div
              className="flex mt-5 border-b-[1px] border-dashed border-[#212529]"
              key={product.order_id}
            >
              <div className="flex items-center gap-5 w-full justify-between">
                <div className="flex gap-5">
                  <img
                    src="https://product.hstatic.net/1000365025/product/xanhcocao1_b494ef3b955942c6bd259aaff5223f65_compact.jpg"
                    alt=""
                  />
                  <div className="flex flex-col gap-2">
                    <p>{product.products.name}</p>
                    <p>E06HI EVER GREEN</p>
                    <p>
                      Số lượng : <>{product?.quantity}</>
                    </p>
                    <p className="text-red-500">
                      {" "}
                      Giá:{" "}
                      <>
                        {parseFloat(product?.products.price).toLocaleString()}
                      </>
                      ₫
                    </p>
                  </div>
                </div>

                <div className="flex flex-col">
                  <h3 className="font-bold">Thành tiền</h3>
                  <div className="flex flex-col items-center justify-center h-full">
                    <p className="text-red-500 font-semibold text-xl">
                      {parseFloat(product?.total_price).toLocaleString()}₫
                    </p>
                  </div>
                </div>
                <div className="flex flex-col items-center">
                  <h3 className="font-bold">Mã QR</h3>
                  <QR amount={product?.total_price} />
                </div>
              </div>
            </div>
          ))
        ) : (
          <div className="flex items-center justify-center">
            <img
              className="img-fluid"
              src="https://file.hstatic.net/200000259653/file/empty-cart_large_46db8e27ff56473ca63e3c4bb8981b64.png"
              alt="Empty cart"
            />
          </div>
        )}
      </div>
    </div>
  );
};
export default CartLayout;
