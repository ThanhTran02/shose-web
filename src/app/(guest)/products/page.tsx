"use client";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";

import { Breadcrumb, Button, Form, FormProps, Radio } from "antd";
import { HomeOutlined, ProductOutlined } from "@ant-design/icons";

import CardProduct from "@/components/carosel/CardProduct";
import "../../../components/carosel/carosel.scss";
import { quanLySanPhamServices } from "@/server";
import { Product } from "@/types";

const ProductsPage = () => {
  const [products, setProducts] = useState<any>();
  const router = useRouter();
  //form search//

  const onFinish: FormProps<any>["onFinish"] = async (values) => {
    console.log("Success:", values);
    let query = "";
    if (+values.categoryId !== 0) query += `categoryId=${values.categoryId}&`;
    if (values.price != 0) {
      switch (+values.price) {
        case 1:
          query += "minPrice=0&maxPrice=500000&";
          break;
        case 2:
          query += "minPrice=500000&maxPrice=1000000&";
          break;
        case 3:
          query += "minPrice=1000000&";
          break;
        default:
          break;
      }
    }
    query += values.sortOrder == 1 ? "sortOrder=asc" : "sortOrder=desc";
    router.push(`/products?${query}`);
    try {
      const { data } = await quanLySanPhamServices.searchProducts(query);
      //@ts-ignore
      setProducts(data.content);
    } catch (error) {
      console.log(error);
    }
  };
  // form search//
  const fetchProducts = async () => {
    try {
      const { data } = await quanLySanPhamServices.getAllProducts();
      //@ts-ignore
      setProducts(data.content);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <div className="mt-5 m-auto w-[1280px] min-h-[500px]">
      <Breadcrumb
        items={[
          {
            title: (
              <Link href={"/"}>
                <HomeOutlined />
                <span>Home</span>
              </Link>
            ),
          },
          {
            title: (
              <>
                <ProductOutlined />
                <span>Tất cả sản phẩm</span>
              </>
            ),
          },
        ]}
      />
      <div className="grid my-5 grid-cols-5">
        <div className="col-span-1">
          <Form
            onFinish={onFinish}
            initialValues={{
              categoryId: 0,
              price: 0,
              sortOrder: 1,
            }}
          >
            <Form.Item name="categoryId">
              <div>
                <h3 className=" font-semibold">Loại sản phẩm</h3>
                <Radio.Group name="categoryId" defaultValue={0}>
                  <Radio value={0}>Tất cả</Radio>
                  <Radio value={1}>Giày Nam</Radio>
                  <Radio value={2}>Giày Nữ</Radio>
                </Radio.Group>
              </div>
            </Form.Item>
            <Form.Item name="price">
              <div>
                <h3 className=" font-semibold">Mức giá</h3>
                <Radio.Group defaultValue={0}>
                  <Radio value={0}>Tất cả</Radio>
                  <Radio value={1}>Dưới 500.000</Radio>
                  <Radio value={2}>500.000-1.000.000</Radio>
                  <Radio value={3}>Trên 1.000.000</Radio>
                </Radio.Group>
              </div>
            </Form.Item>
            <Form.Item name="sortOrder">
              <div>
                <h3 className="font-semibold">Sắp xếp theo</h3>
                <Radio.Group defaultValue={1}>
                  <Radio value={1}>Tăng dần</Radio>
                  <Radio value={2}>Giảm dần</Radio>
                </Radio.Group>
              </div>
            </Form.Item>
            <Form.Item>
              <Button type="dashed" htmlType="submit" style={{ width: 200 }}>
                Lọc
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="col-span-4">
          <div className="grid grid-cols-4 bg-[#f4f4f4]">
            {products?.map((product: Product) => (
              <div key={product.product_id}>
                <CardProduct product={product} />
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProductsPage;
