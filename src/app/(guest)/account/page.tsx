import { Breadcrumb } from "antd";
import React from "react";
import { HomeOutlined, UserOutlined } from "@ant-design/icons";
import AccountSideBar from "@/components/account/account.page.slidebar";
import AccountPageContent from "@/components/account/account.page.content";
import Link from "next/link";
const AccountPage = () => {
  return <AccountPageContent />;
};

export default AccountPage;
