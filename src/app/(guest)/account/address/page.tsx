"use client";
import React from "react";

const AccountAddressPage = () => {
  return (
    <div className="w-full account_content border-[1px] border-black min-h-[300px]">
      <h2 className=" bg-black text-white py-2 px-6 text-xl  ">
        Danh sách địa chỉ
      </h2>
      <div className="table-responsive p-2"></div>
    </div>
  );
};

export default AccountAddressPage;
