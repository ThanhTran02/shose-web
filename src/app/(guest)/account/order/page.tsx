"use client";

import { useAuth } from "@/hook";
import { quanLyNguoiDungServices } from "@/server";
import { Product } from "@/types";
import { Table, TableProps } from "antd";
import { useEffect, useState } from "react";

const AccountOrderPage = () => {
  const [products, setProducts] = useState([]);
  const { user } = useAuth();
  const fetchData = async () => {
    const { data } = await quanLyNguoiDungServices.getPurchaseHistory(
      //@ts-ignore
      user?.user_id
    );
    //@ts-ignore
    const dataForm = data.content.map((item, index) => ({
      key: index,
      name: item.name,
      typeProduct: item.categories ? item.categories.name : "",
      price: item.price,
      quantity: item.quantity,
    }));
    setProducts(dataForm);
  };
  useEffect(() => {
    fetchData();
  }, []);
  console.log(products);
  const columns: TableProps<any>["columns"] = [
    {
      title: "Tên sản phẩm",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Loại sản phẩm",
      dataIndex: "typeProduct",
      key: "typeProduct",
    },
    {
      title: "Giá sản phẩm",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
    },
  ];

  return (
    <div className="w-full account_content border-[1px] border-black min-h-[450px] ">
      <h2 className=" bg-black text-white py-2 px-6 text-xl  ">
        Lịch sử mua hàng
      </h2>
      <div className="table-responsive p-2">
        {products?.length == 0 ? (
          <p>Bạn chưa đặt mua sản phẩm nào! </p>
        ) : (
          <div>
            <Table
              columns={columns}
              dataSource={products}
              pagination={{ pageSize: 5 }}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default AccountOrderPage;
