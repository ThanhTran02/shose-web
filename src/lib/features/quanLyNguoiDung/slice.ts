import { createSlice } from "@reduxjs/toolkit";

type QuanLyNguoiDungInitailState = {
  user?: any;
  isUpdatingUser?: boolean;
};
const initialState: QuanLyNguoiDungInitailState = {
  user: undefined,
  // isUpdatingUser: false,
};
export const quanLiNguoiDungSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {
    logOut: (state) => {
      localStorage.removeItem("token");
      localStorage.removeItem("id");
      state.user = undefined;
    },
    // getUserInfo
  },
});
export const {
  reducer: quanLyNguoiDungReducer,
  actions: quanLyNguoiDungActions,
} = quanLiNguoiDungSlice;
