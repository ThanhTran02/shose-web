import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyNguoiDungServices = {
  getPurchaseHistory: async (id: string) =>
    api.get<NextApiResponse<any>>(`./orders/history/${id}`),
};
