import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLySanPhamServices = {
  getAllProducts: async () => api.get<NextApiResponse<any>>(`./products`),
  getProductsByName: async (name: string) =>
    api.get<NextApiResponse<any>>(`./products/${name}`),
  searchProducts: async (query: string) =>
    api.get<NextApiResponse<any>>(`./products/search?${query}`),
};
