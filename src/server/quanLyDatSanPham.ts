import { apiInstance } from "@/constant/apiInstance";
import { NextApiResponse } from "next";

const api = apiInstance({
  baseURL: process.env.NEXT_PUBLIC_BE_URL,
});
export const quanLyDatSanPhamServices = {
  bookingProducts: async (data: any) =>
    api.post<NextApiResponse<any>>(`./orders`, data),
  getAllOrders: async (id: string) =>
    api.get<NextApiResponse<any>>(`./orders/${id}`),
};
